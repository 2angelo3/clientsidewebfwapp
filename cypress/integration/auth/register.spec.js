// <reference types="cypress"/>

describe('Register', () => {
  beforeEach(() => {
    // Start a mock server
    cy.server();

    // Visit the login page of the application
    cy.visit('http://localhost:4200/login');
  });

  it('should be able to login', () => {
    cy.route('POST', '**/login', 'fixture:login.json')

    cy.get('#userEmail').type('pop@gmail.com')
    cy.get('#userPassword').type('pop123')

    cy.get('.btn').contains('Submit').click();
  })


})
