/// <reference types="cypress"/>

describe('CRUD Wishlist', () => {
  beforeEach(() => {
    cy.server();

    cy.visit('http://localhost:4200/login');

    cy.route('POST', '**/login', 'fixture:login.json')

    cy.get('#userEmail').type('pop@gmail.com')
    cy.get('#userPassword').type('pop123')

    cy.get('.btn').contains('Submit').click();
  })

  it('should be able to create new wishstlist', () => {
    cy.route('GET', '**/wishlist', 'fixture:wishlist-list.json').as('wishlistList')
    cy.route('GET', '**/equipment', 'fixture:equipment-list.json')

    cy.get(':nth-child(4) > .nav-link').click()

    cy.wait('@wishlistList')
    cy.get('@wishlistList').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body).to.have.length(2);
    });

    cy.get('.btn').click();

    cy.route('POST', '**/wishlist', 'fixture:wishlist-create.json').as('createWishlist');

    cy.get('#wishlistName').type("Gewichtenlijstje");
    cy.get('form.ng-untouched > :nth-child(2) > .form-control').select('avans');
    cy.get('#wishlistDescription').type('Zwaargewicht');

    cy.get('.btn').click();

    cy.wait('@createWishlist');
    cy.get('@createWishlist').then(xhr => {
      expect(xhr.response.body).to.haveOwnProperty('name').equals('Gewichtenlijstje');
      expect(xhr.response.body).to.haveOwnProperty('equipment').equals('605a6a73c4ad72129484cd5a');
      expect(xhr.response.body).to.haveOwnProperty('description').equals('Zwaargewicht');
    })
  })

  it('should read detail from an wishlist ', () => {
    cy.route('GET', '**/wishlist', 'fixture:wishlist-list.json')
    cy.route('GET', '**/equipment', 'fixture:equipment-list.json')

    cy.get(':nth-child(4) > .nav-link').click()



    cy.get(':nth-child(1) > :nth-child(1) > #wishlistbtn').click()







  });


  it('Edit wishlist', () => {

    cy.route('GET', '**/wishlist', 'fixture:wishlist-list.json').as('wishlistList')
    cy.route('GET', '**/equipment', 'fixture:equipment-list.json')

    cy.get(':nth-child(4) > .nav-link').click()

    cy.wait('@wishlistList')
    cy.get('@wishlistList').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body).to.have.length(2);
    });

    cy.route('GET', '**/wishlist/605f8aeb2c454233a8be32d3', 'fixture:wishlist-detail.json')

    cy.get(':nth-child(1) > :nth-child(1) > #wishlistbtn').click();

    cy.get('.btn-outline-warning').click();

    cy.get('#wishlistName').clear().type('Not Holly Molly');
    cy.get(':nth-child(3) > .form-control').select('avans');
    cy.get('#wishlistDescription').clear().type('Changed to something else');

    cy.route('PUT', '**/wishlist/605f8aeb2c454233a8be32d3', 'fixture:wishlist-edit.json').as('editedWishlist');

    cy.get('.btn').click()

    cy.wait('@editedWishlist');
    cy.get('@editedWishlist').then(xhr => {
      expect(xhr.status).to.equal(200);
      expect(xhr.response.body).to.haveOwnProperty('name')
      expect(xhr.response.body).to.haveOwnProperty('equipment')
      expect(xhr.response.body).to.haveOwnProperty('description')
    })

  });
})
