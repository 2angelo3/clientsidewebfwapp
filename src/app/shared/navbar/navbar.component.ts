import { Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from 'src/app/pages/user/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  @Input() title: string;
  isNavbarCollapsed = true;
  loggedInUser$: BehaviorSubject<User>;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {

    console.log(this.authService.currentUser$.getValue())

  }


  logout(): void {
  this.authService.doLogout();
  }
}
