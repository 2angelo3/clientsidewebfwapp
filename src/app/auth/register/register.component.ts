import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/pages/user/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy{
  user: User;
  name: string;
  email: string;
  description: string;
  password: string;
  subscription: Subscription;

  constructor(private router: Router, private authService: AuthService) { }
  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }

  ngOnInit(): void {

    this.user = new User(  this.name,
       this.email,
       this.password,
       this.description)


  }

  register(): void {




    this.subscription = this.authService.registerUser(this.user).subscribe(user => {
      this.user = user;
      this.router.navigate(['/dashboard']);
    });
  }


}
