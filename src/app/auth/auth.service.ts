import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { User } from '../pages/user/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private headers = {'content-type': 'application/json'};

  constructor(private http: HttpClient) { }

  public currentUser$ = new BehaviorSubject<User>(undefined);

  private readonly CURRENT_USER = 'currentUser';

  private safeToLocalStorage(user: User): void {
    localStorage.setItem(this.CURRENT_USER, JSON.stringify(user));
  }

  getUserFromLocalStorage(): Observable<User> {
    const localUser = JSON.parse(localStorage.getItem(this.CURRENT_USER));
    return of(localUser);
  }


  registerUser(user: User): Observable<User> {
    console.log("log de user " + user.username)

      return this.http.post(`${environment.apiUrl}user/signup`, user, {headers: this.headers}).pipe(
        map((response: any) => {
          // tslint:disable-next-line: no-shadowed-variable
          const user = {...response} as User;
          this.safeToLocalStorage(user);
          this.currentUser$.next(user);
          return user;
        })
        );
      }

      loginUser(email: string, password: string): Observable<User> {
        console.log("logs in " + email )
        console.log("logs in " + password )
        return this.http.post(`${environment.apiUrl}user/login`, {email: email, password: password}, {headers: this.headers}).pipe(
          map((response: any) => {
      const user = {...response} as User;
      console.log(user.email)
      this.safeToLocalStorage(user);
      this.currentUser$.next(user);
      return user;
    })
  );
  }


  doLogout(): void {
    localStorage.clear();
    this.currentUser$.next(undefined);
  }

  userMayEdit(itemUserId: string): Observable<boolean> {
    console.log('User may edit is called')
    return this.currentUser$.pipe(
      map((user: User) => (user? user._id === itemUserId : false))
    );
  }


   isLoggedIn(): boolean {
    let authToken = localStorage.getItem('currentUser');
    return (authToken !== null) ? true : false;
  }



}
