import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from 'src/app/pages/user/user.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy{
  user: User;
  email: string;
  password: string;
  subscription: Subscription;
  errorThrown = false;


  constructor(private router: Router, private authservice: AuthService) {}
  ngOnDestroy(): void {
    if (this.subscription) this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.email = '';
    this.password = '';
    this.user = new User(  null,
      this.email,
      this.password,
      null)



  }

  login(): void {


    this.subscription = this.authservice.loginUser(this.user.email, this.user.password).subscribe(user => {
      if(user !== undefined) {
      this.user = user;
      this.router.navigate(['/dashboard']);

      } else {
        console.log('error thrown');
        this.errorThrown = true;
      }  });



  }
}
