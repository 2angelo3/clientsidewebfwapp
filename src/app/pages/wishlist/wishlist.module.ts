import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WishlistListComponent } from './wishlist-list/wishlist-list.component';
import { WishlistEditComponent } from './wishlist-edit/wishlist-edit.component'
import { WishlistAddComponent } from './wishlist-add/wishlist-add.component';
import { WishlistDetailComponent } from './wishlist-detail/wishlist-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../../auth/auth.guard';


const routes: Routes = [

  {
    path: 'editWishlist/:id', pathMatch: 'full', canActivate: [AuthGuard], component: WishlistEditComponent
  },
  {
    path: 'add', pathMatch: 'full', canActivate: [AuthGuard], component: WishlistAddComponent
  },
  {
    path: '', pathMatch: 'full', component: WishlistListComponent
  },
  {
    path: ':id', pathMatch: 'full', component: WishlistDetailComponent
  }

];


@NgModule({
  declarations: [WishlistListComponent, WishlistEditComponent, WishlistAddComponent,  WishlistDetailComponent],
  imports: [
    CommonModule, FormsModule, RouterModule.forChild(routes)
  ]
})
export class WishlistModule { }
