import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../../equipment/equipment.model';
import { EquipmentService } from '../../equipment/equipment.service';
import { WishlistService } from '../../wishlist.service';
import { Wishlist } from '../wishlist.model';

@Component({
  selector: 'app-wishlist-edit',
  templateUrl: './wishlist-edit.component.html',
  styleUrls: ['./wishlist-edit.component.css']
})
export class WishlistEditComponent implements OnInit, OnDestroy {

  wishlist: Wishlist;
  subscription: Subscription;
  equipments$: Observable<Equipment[]>


  constructor(private wishlistService: WishlistService, private route: ActivatedRoute, private router: Router, public authService: AuthService, private equipmentService: EquipmentService) { }
  ngOnDestroy(): void {
    if(this.subscription) this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    this.equipments$ = this.equipmentService.getEquipment();

    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.wishlistService.getWishlistById(params.get('id'));
      })
    ).pipe(

    ).subscribe(data => {
      this.wishlist = data;
    });


  }


  updateWishlist(): void {
   this.subscription = this.wishlistService.updateWishlist(this.wishlist).subscribe(wishlists => {
    console.log(this.wishlist._id);
    this.router.navigate(['/wishlist']);

  });
}

}
