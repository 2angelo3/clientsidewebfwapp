import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { delay, switchMap, tap } from 'rxjs/operators';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../../equipment/equipment.model';
import { EquipmentService } from '../../equipment/equipment.service';
import { WishlistService } from '../../wishlist.service';
import { Wishlist } from '../wishlist.model';

@Component({
  selector: 'app-wishlist-detail',
  templateUrl: './wishlist-detail.component.html',
  styleUrls: ['./wishlist-detail.component.css']
})
export class WishlistDetailComponent implements OnInit {

  wishlist$: Observable<Wishlist>;
  wishlists$: Observable<Wishlist[]>;
  wishlist: Wishlist;
  paramSubscription: Subscription;


  constructor(private userService: WishlistService, private route: ActivatedRoute, private router: Router, public authService: AuthService, public logged : AuthGuard
    ) { }


  ngOnInit(): void {

    this.wishlist$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.userService.getWishlistById(params.get('id'))
      )

    );
  }

  getWishlist(): void {
    this.wishlists$ = this.userService.getWishlist();
    }


  deleteWishlist(wishlist: Wishlist): void {
    this.userService.deleteWishlist(wishlist).subscribe(() => {
      console.log(wishlist);
      this.getWishlist();
      this.router.navigate(['/wishlist']);
      //  alert(`Wishlist ${this.wishlist.name} was successfully deleted`)
    });
  }


  tryToEdit(wishlist: Wishlist){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(wishlist.user)){
        console.log(' allowed');
        this.router.navigateByUrl('wishlist/editWishlist/' + wishlist._id);
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value.email));
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log(wishlist._id)
        this.router.navigateByUrl('/login');
      }
    }
  }

  tryToDelete(wishlist: Wishlist){

    console.log(wishlist.user._id)
    console.log( JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(wishlist.user));
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(wishlist.user)){
        console.log(' allowed');
        this.deleteWishlist(wishlist
          );
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value.email));
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log(JSON.stringify(wishlist.equipment.emailAddress))
        this.router.navigateByUrl('/login');
      }
    }
  }



}
