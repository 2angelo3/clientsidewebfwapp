import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { AuthService } from 'src/app/auth/auth.service';
import { WishlistService } from '../../wishlist.service';
import { Wishlist } from '../wishlist.model';
import { WishlistModule } from '../wishlist.module';

@Component({
  selector: 'app-wishlist-list',
  templateUrl: './wishlist-list.component.html',
  styleUrls: ['./wishlist-list.component.css']
})
export class WishlistListComponent implements OnInit {

  wishlist$: Observable<Wishlist[]>;


  constructor(private wishlistService: WishlistService, public authService: AuthService) { }

  ngOnInit(): void {
    console.log('userlist is loaded');
    // calls the method and show the users
    this.getWishlist();
  }


  getWishlist(): void {
    this.wishlist$ = this.wishlistService.getWishlist();
  }

  deleteWishlist(wishlist: Wishlist): void {
    this.wishlistService.deleteWishlist(wishlist).subscribe(wishlists => {
      console.log(wishlists);
      this.getWishlist();
    });
  }






}
