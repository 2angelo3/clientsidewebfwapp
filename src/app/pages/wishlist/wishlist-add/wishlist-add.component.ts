import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../../equipment/equipment.model';
import { EquipmentService } from '../../equipment/equipment.service';
import { WishlistService } from '../../wishlist.service';
import { Wishlist } from '../wishlist.model';

@Component({
  selector: 'app-wishlist-add',
  templateUrl: './wishlist-add.component.html',
  styleUrls: ['./wishlist-add.component.css']
})
export class WishlistAddComponent implements OnInit {

  wishlist: Wishlist;
  equipments$: Observable<Equipment[]>;

  constructor(private wishlistService: WishlistService, private route: Router, private equipmentService: EquipmentService, public authService: AuthService) { }

  ngOnInit(): void {
    this.equipments$ = this.equipmentService.getEquipment();

    this.wishlist= {
      user: null,
      name: "",
      description: "",
      equipment: null
    }
  }



  createWishlistMethod(): void {
    this.wishlist.user = this.authService.currentUser$.getValue()

    console.log(this.wishlist.user)


    console.log('create wishlist called');
    console.log(this.wishlist)
    this.wishlistService.createWishlist(this.wishlist)
    .subscribe(data => {
      console.log(data);
      this.route.navigate(['/wishlist']);
      alert(`Wishlist ${this.wishlist.name} was successfully created`)
    });
  }
}
