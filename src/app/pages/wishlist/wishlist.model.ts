import { Equipment } from "../equipment/equipment.model";
import { User } from "../user/user.model";

export class Wishlist {

  _id?: string;
  name: string;
  description: string;
  equipment: Equipment;
  user: User;

}
