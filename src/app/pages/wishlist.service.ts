import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Wishlist } from './wishlist/wishlist.model';

const baseUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class WishlistService {


  constructor(private http: HttpClient) { }



  getWishlist(): Observable<Wishlist[]> {
    return this.http.get<Wishlist[]>(baseUrl + 'wishlist');
  }

  getWishlistById(id: string): Observable<Wishlist> {
    return this.http.get<Wishlist>(baseUrl + `wishlist/${id}`);
  }

  createWishlist(wishlist: Wishlist): Observable<Wishlist> {
    const headers = { 'content-type': 'application/json'};
    return this.http.post<Wishlist>(baseUrl + 'wishlist', wishlist, {headers});
  }

  updateWishlist(wishlist: Wishlist): Observable<Wishlist> {
    const headers = { 'content-type': 'application/json'};
    console.log('id = ' + wishlist._id);
    return this.http.put<Wishlist>(baseUrl + `wishlist/${wishlist._id}`, wishlist, {headers});
  }

  deleteWishlist(wishlist: Wishlist): Observable<Wishlist> {
    return this.http.delete<Wishlist>(baseUrl + `wishlist/${wishlist._id}`);
  }

}


