export class User {

  token?: string;
  _id: string;
  username: string;
  password: string;
  email: string;
  description: string;

  constructor(username: string, email: string, password: string, description: string){
    this.username = username;
    this.email = email;
    this.password = password;
    this.description =description;

  }
}



