import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import * as fromComponents from '.';
import { FormsModule } from '@angular/forms';



const routes: Routes = [
  { path: '', pathMatch: 'full', component: fromComponents.UserListComponent },
  { path: ':id', pathMatch: 'full', component: fromComponents.UserDetailComponent },
  { path: 'user/:id', pathMatch: 'full', component: fromComponents.UserEditComponent }
];

@NgModule({
  declarations: [...fromComponents.components],
  providers: [],
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule],
})
export class UserModule { }





