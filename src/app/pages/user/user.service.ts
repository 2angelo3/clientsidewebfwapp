import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';
import { User } from './user.model';
import { delay, filter, take } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

const baseUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class UserService  {
  constructor(private http: HttpClient) {

   }


   getUsers(): Observable<User[]> {
    return this.http.get<User[]>(baseUrl + 'user');
  }

  getUserById(id: string): Observable<User> {
    return this.http.get<User>(baseUrl + `user/${id}`);
  }

  createUser(user: User): Observable<User> {
    const headers = { 'content-type': 'application/json'};
    return this.http.post<User>(baseUrl + 'user', user, {headers});
  }

  updateUser(user: User): Observable<User> {
    const headers = { 'content-type': 'application/json'};
    console.log('id = ' + user._id);
    return this.http.put<User>(baseUrl + `user/${user._id}`, user, {headers});
  }

  deleteUser(user: User): Observable<User> {

    return this.http.delete<User>(baseUrl + `user/${user._id}`);
  }


}

