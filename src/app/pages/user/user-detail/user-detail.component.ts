import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { delay, switchMap } from 'rxjs/operators';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { Equipment } from '../../equipment/equipment.model';
import { AuthService } from 'src/app/auth/auth.service';



@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {
  user$: Observable<User>;
  users$: Observable<User[]>;
  user: User;
  paramSubscription: Subscription;


  constructor(private userService: UserService, private route: ActivatedRoute, private router: Router, public authService: AuthService , public logged : AuthGuard) { }


  ngOnInit(): void {

    this.user$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.userService.getUserById(params.get('id'))
      )

    );
  }

  getUsers(): void {
    this.userService.getUsers().subscribe(users => {
      console.log(users);
      this.users$ = this.userService.getUsers();
    });
    }


  deleteUser(user: User): void {
    this.userService.deleteUser(user).subscribe(users => {
      console.log(users);
      this.getUsers();
      this.router.navigate(['/user']);
    });
  }


  tryToEdit(equipment: Equipment){
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value.email) === JSON.stringify(equipment.emailAddress)){
        console.log(' allowed');
        this.router.navigateByUrl('equipment/editEquipment/' + equipment._id);
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value.email));
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log(equipment.emailAddress)
        this.router.navigateByUrl('/login');
      }
    }
  }

  tryToDelete(user: User){

    console.log(user.email)
    console.log( JSON.stringify(this.authService.currentUser$.value.email) === JSON.stringify(user.email));
    if(this.logged.canActivate() === false){
      this.logged.canActivate();
    }else{
      if(JSON.stringify(this.authService.currentUser$.value.email) === JSON.stringify(user.email)){
        console.log(' allowed');
        this.deleteUser(user
          );
      }else{
        console.log('Not allowed');
        console.log(JSON.stringify(this.authService.currentUser$.value.email));
        console.log(JSON.stringify(this.authService.currentUser$.value._id));
        console.log(JSON.stringify(user.email))
        this.router.navigateByUrl('/login');
      }
    }
  }



}

