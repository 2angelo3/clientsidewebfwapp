import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/auth/auth.service';
import { User } from '../user.model';
import { UserService } from '../user.service';

import { UserEditComponent } from './user-edit.component';

const user: User = {
  token: "FASFNJNAK",
  _id: "1",
  username: "Jos",
  password: "lolw",
  email: "jos@gmail.com",
  description: "TTTTTTTTTT",
}


describe('UserEditComponent', () => {
  let component: UserEditComponent;
  let fixture: ComponentFixture<UserEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserEditComponent ],
      providers: [ UserService],
       imports: [ HttpClientTestingModule, RouterTestingModule , FormsModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });
});
