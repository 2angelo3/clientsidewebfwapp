import {​​​​​Component, Input, OnDestroy, OnInit}​​​​​ from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from 'src/app/pages/user/user.model';
import { UserService } from '../user.service';


@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit, OnDestroy {
 user: User;
 subscription: Subscription;

 constructor(private userService: UserService, private route: ActivatedRoute, private router: Router) { }
 ngOnDestroy(): void {
   if(this.subscription) this.subscription.unsubscribe();
 }

  ngOnInit(): void {

  this.subscription = this.route.paramMap.pipe(
    switchMap((params: ParamMap) => {
      return this.userService.getUserById(params.get('id'));
    })
  ).pipe(

  ).subscribe(data => {
    this.user = data;
  });


}


updateUser(): void {
 this.subscription = this.userService.updateUser(this.user).subscribe(users => {
  console.log(this.user._id);
  this.router.navigate(['/user']);

});
}




}
