import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserAddComponent } from './user-add/user-add.component';




export const components: any[] = [UserDetailComponent, UserListComponent, UserEditComponent, UserAddComponent];

export * from './user-detail/user-detail.component';
export * from './user-list/user-list.component';
export * from './user-edit/user-edit.component';
export * from './user-add/user-add.component';

