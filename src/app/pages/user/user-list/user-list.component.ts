import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})

export class UserListComponent implements OnInit, OnDestroy {
  users$: Observable<User[]>;
  user$: Observable<User>;


  constructor(private userService: UserService, private router: Router, private route: ActivatedRoute) {}
  ngOnDestroy(): void {

  }


  ngOnInit(): void {
    console.log('UserList geladen');

    this.user$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
        this.userService.getUserById(params.get('id')
        )
        ));

    this.getUsers();
  }



  getUsers(): void {
    this.userService.getUsers().subscribe(users => {
      console.log(users);
      this.users$ = this.userService.getUsers();
    });
    }

}
