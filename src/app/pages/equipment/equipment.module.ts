import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { EquipmentAddComponent } from './equipment-add/equipment-add.component';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { EquipmentDetailComponent } from './equipment-detail/equipment-detail.component';
import { EquipmentEditComponent } from './equipment-edit/equipment-edit.component';
import { AuthGuard } from '../../auth/auth.guard';

const routes: Routes = [

  {
    path: 'editEquipment/:id', pathMatch: 'full', canActivate: [AuthGuard], component: EquipmentEditComponent
  },
  {
    path: 'add', pathMatch: 'full', canActivate: [AuthGuard], component: EquipmentAddComponent
  },
  {
    path: '', pathMatch: 'full', component: EquipmentListComponent
  },
  {
    path: ':id', pathMatch: 'full', component: EquipmentDetailComponent
  }

];

@NgModule({
  declarations: [EquipmentDetailComponent, EquipmentEditComponent, EquipmentListComponent, EquipmentAddComponent],
  providers: [],
  imports: [ CommonModule, FormsModule, RouterModule.forChild(routes)]
})
export class EquipmentModule { }



