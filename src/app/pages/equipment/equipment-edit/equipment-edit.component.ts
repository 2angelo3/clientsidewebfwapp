import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap} from '@angular/router';
import { from, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Equipment } from '../equipment.model';
import { EquipmentService } from '../equipment.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-equipment-edit',
  templateUrl: './equipment-edit.component.html',
  styleUrls: ['./equipment-edit.component.css']
})
export class EquipmentEditComponent implements OnInit, OnDestroy {
  equipment: Equipment;
  subscription: Subscription;



  constructor(private equipmentService: EquipmentService, private route: ActivatedRoute, private router: Router, public authService: AuthService) { }
  ngOnDestroy(): void {
   if(this.subscription) this.subscription.unsubscribe();
  }

  ngOnInit(): void {


    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.equipmentService.getEquipmentById(params.get('id'));
      })
    ).pipe(

    ).subscribe(data => {
      this.equipment = data;
    });


  }


  updateEquipment(): void {
   this.subscription = this.equipmentService.updateEquipment(this.equipment).subscribe(equipments => {
    console.log(this.equipment._id);
    this.router.navigate(['/equipment']);

  });
}

}
