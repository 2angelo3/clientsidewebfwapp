import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { User } from '../../user/user.model';
import { Equipment } from '../equipment.model';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { AuthService } from '../../../auth/auth.service';
import { Router, ActivatedRoute, convertToParamMap } from '@angular/router';
import { EquipmentService } from '../equipment.service';
import { EquipmentEditComponent } from './equipment-edit.component';



const expectedEquipment: Equipment = {
  _id: '3',
  name: 'item',
  emailAddress: 'pop@gmail.',
  description: 'itemmetmsfa',
  comments: null,
  postedBy: {
    _id: '1',
    username: "test",
    email: 'test@test.com',
      password: 'test',
      description: 'test'
  }
};


const updateEquipment: Equipment = {
  _id: '3',
  name: 'Dumbelstang',
  emailAddress: 'dumbell@gmail.com',
  description: '10 kg',
  comments: null,
  postedBy: {
    _id: '1',
    username: "test",
    email: 'test@test.com',
      password: 'test',
      description: 'test'
  },
};

const expectedUserData: User = {
  _id: '1',
  username: 'test',
  password: 'test',
  email: 'test@test.com',
  description: 'test'

};

describe('EquipmentEditComponent', () => {
  let component: EquipmentEditComponent;
  let fixture: ComponentFixture<EquipmentEditComponent>;

  let equipmentServiceSpy;
  let authServiceSpy;
  let routerSpy;

  beforeEach(() => {

    authServiceSpy = jasmine.createSpyObj(
      'AuthService',
      [
        'login',
        'register',
        'logout',
        'getUserFromLocalStorage',
        'saveUserToLocalStorage',
        'userMayEdit',
      ]
      // ['currentUser$']
    );
    const mockUser$ = new BehaviorSubject<User>(expectedUserData);
    authServiceSpy.currentUser$ = mockUser$;

    equipmentServiceSpy = jasmine.createSpyObj('EquipmentService', ['createEquipment', 'getEquipment', 'getEquipmentById', 'updateEquipment', 'deleteEquipment']);
    routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);

    TestBed.configureTestingModule({
      // The declared components needed to test the UsersComponent.
      declarations: [
        EquipmentEditComponent, // The 'real' component that we will test

      ],
      imports: [FormsModule, RouterTestingModule, HttpClientTestingModule],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real service in testcases!
      //
      providers: [
        { provider: AuthService, useValue: authServiceSpy },
        // { provide: AlertService, useValue: alertServiceSpy },
        { provider: Router, useValue: routerSpy },
        { provider: EquipmentService, useValue: equipmentServiceSpy },
        // { provide: EquipmentService, useValue: EquipmentServiceSpy },
        {
          provider: ActivatedRoute,
          useValue: {paramMap: of(convertToParamMap({id: '1'}))}
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(EquipmentEditComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });




// describe('EquipmentEditComponent', () => {
//   let component: EquipmentEditComponent;
//   let fixture: ComponentFixture<EquipmentEditComponent>;

//   beforeEach(async () => {
//     await TestBed.configureTestingModule({
//       declarations: [ EquipmentEditComponent ]
//     })
//     .compileComponents();
//   });

//   beforeEach(() => {
//     fixture = TestBed.createComponent(EquipmentEditComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });



  it('should have an equipment and is logged on, edit succesfull', (done) => {

    equipmentServiceSpy.getEquipmentById.withArgs('5').and.returnValue(of(expectedEquipment));
    equipmentServiceSpy.updateEquipment.withArgs(expectedEquipment._id, expectedEquipment).and.returnValue(of(updateEquipment));
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));

    component.subscription = equipmentServiceSpy.getEquipmentById('5').subscribe(data => {
      component.equipment = data;
    });

    fixture.detectChanges();

    expect(component.equipment).toEqual(expectedEquipment);
    expect(authServiceSpy.currentUser$.value).toEqual(expectedUserData);

    component.equipment.name = 'Dumbelstang';
    component.equipment.description = '10 kg';
    component.equipment.emailAddress = 'dumbell@gmail.com';

    fixture.detectChanges();

    component.subscription = equipmentServiceSpy.updateEquipment(component.equipment._id, component.equipment).subscribe(equipment => {
      component.equipment =equipment;
    });

    fixture.detectChanges();
    setTimeout(() => {
      expect(component.equipment).toEqual(updateEquipment);
      expect(expectedEquipment).toEqual(updateEquipment);
      done();
    }, 200);
  });


  it('equipment does not exist', () => {
    equipmentServiceSpy.getEquipmentById.withArgs('500').and.returnValue(of(undefined));
    authServiceSpy.getUserFromLocalStorage.and.returnValue(of(expectedUserData));



    component.subscription = equipmentServiceSpy.getEquipmentById('500').subscribe(data => {
      component.equipment = data;
    });

    fixture.detectChanges();
    expect(component.equipment).toBeFalsy();
      });

});
