import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../equipment.model';
import { EquipmentService } from '../equipment.service';

@Component({
  selector: 'app-equipment-list',
  templateUrl: './equipment-list.component.html',
  styleUrls: ['./equipment-list.component.css']
})
export class EquipmentListComponent implements OnInit {
  equipments$: Observable<Equipment[]>;

  constructor(private equipmentService: EquipmentService, public authService: AuthService) { }

  ngOnInit(): void {
    console.log('userlist is loaded');
    // calls the method and show the users
    this.getEquipment();
  }


  getEquipment(): void {
    this.equipments$ = this.equipmentService.getEquipment();
  }

}
