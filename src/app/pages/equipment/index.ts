import { EquipmentAddComponent } from './equipment-add/equipment-add.component';
import { EquipmentListComponent } from './equipment-list/equipment-list.component';
import { EquipmentDetailComponent } from './equipment-detail/equipment-detail.component';
import { EquipmentEditComponent } from './equipment-edit/equipment-edit.component';

export const components: any[] = [
  EquipmentDetailComponent,
  EquipmentEditComponent,
  EquipmentListComponent,
  EquipmentAddComponent,
];

export * from './equipment-add/equipment-add.component';
export * from './equipment-list/equipment-list.component';
export * from './equipment-detail/equipment-detail.component';
export * from './equipment-edit/equipment-edit.component';
