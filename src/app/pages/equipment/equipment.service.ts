import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Equipment } from './equipment.model';

const baseUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {

  constructor(private http: HttpClient) { }

  getEquipment(): Observable<Equipment[]> {
    return this.http.get<Equipment[]>(baseUrl + 'equipment');
  }

  getEquipmentById(id: string): Observable<Equipment> {
    return this.http.get<Equipment>(baseUrl + `equipment/${id}`);
  }

  createEquipment(equipment: Equipment): Observable<Equipment> {
    const headers = { 'content-type': 'application/json'};

    console.log(baseUrl);

    return this.http.post<Equipment>(baseUrl + 'equipment', equipment, {headers});
  }

  updateEquipment(equipment: Equipment): Observable<Equipment> {
    const headers = { 'content-type': 'application/json'};
    console.log('id = ' + equipment._id);
    return this.http.put<Equipment>(baseUrl + `equipment/${equipment._id}`, equipment, {headers});
  }

  deleteEquipment(equipment: Equipment): Observable<Equipment> {
    const headers = { 'content-type': 'application/json'};
    console.log('id = ' + equipment._id);
    return this.http.delete<Equipment>(baseUrl + `equipment/${equipment._id}`);
  }

  getDropdownvalues(): Observable<any> {
    return this.http.get<Comment[]>(baseUrl + `equipment`);

  }
}


