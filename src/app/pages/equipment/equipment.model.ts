import { User } from "../user/user.model";

export class Equipment {
  _id?: string;
  name: string;
  emailAddress: string;
  description: string;
  comments: Comment[];
  postedBy: User;
}
