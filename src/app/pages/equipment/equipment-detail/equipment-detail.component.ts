import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthGuard } from 'src/app/auth/auth.guard';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../equipment.model';
import { EquipmentService } from '../equipment.service';



@Component({
  selector: 'app-equipment-detail',
  templateUrl: './equipment-detail.component.html',
  styleUrls: ['./equipment-detail.component.css']
})
export class EquipmentDetailComponent implements OnInit, OnDestroy {
  equipments$: Observable<Equipment[]>;
  equipment$: Observable<Equipment>;
  equipment: Equipment;
  subscription: Subscription;

  constructor(private equipmentService: EquipmentService , private route: ActivatedRoute, private router: Router, public authService: AuthService, public logged : AuthGuard) { }
  ngOnDestroy(): void {
    if(this.subscription) this.subscription.unsubscribe();
  }

  ngOnInit(): void {


  this.equipment$ = this.route.paramMap.pipe(
    switchMap((params: ParamMap) =>
      this.equipmentService.getEquipmentById(params.get('id')
      )
      ));

      }

      getEquipment(): void {
        this.equipments$ = this.equipmentService.getEquipment();
      }



      deleteEquipment(equipment: Equipment): void {
        this.subscription = this.equipmentService.deleteEquipment(equipment).subscribe(equipments => {
          console.log(equipments);
          this.getEquipment();
          this.router.navigate(['/equipment']);
        });
      }


      tryToEdit(equipment: Equipment){
        if(this.logged.canActivate() === false){
          this.logged.canActivate();
        }else{
          if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(equipment.postedBy)){
            console.log(' allowed');
            this.router.navigateByUrl('equipment/editEquipment/' + equipment._id);
          }else{
            console.log('Not allowed');
            console.log(JSON.stringify(this.authService.currentUser$.value.email));
            console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(equipment.emailAddress)
            this.router.navigateByUrl('/login');
          }
        }
      }

      tryToDelete(equipment: Equipment){

        console.log(equipment.postedBy)
        console.log( JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(equipment.postedBy));
        if(this.logged.canActivate() === false){
          this.logged.canActivate();
        }else{
          if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(equipment.postedBy)){
            console.log(' allowed');
            this.deleteEquipment(equipment
              );
          }else{
            console.log('Not allowed');
            console.log(JSON.stringify(this.authService.currentUser$.value.email));
            console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(JSON.stringify(equipment.emailAddress))
            this.router.navigateByUrl('/login');
          }
        }
      }

  }
