import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { Equipment } from '../equipment.model';
import { EquipmentService } from '../equipment.service';

@Component({
  selector: 'app-equipment-add',
  templateUrl: './equipment-add.component.html',
  styleUrls: ['./equipment-add.component.css']
})
export class EquipmentAddComponent implements OnInit, OnDestroy {

  equipment: Equipment
  equipments = new Equipment();

  subscription: Subscription;

  constructor(private equipmentService: EquipmentService, private location: Location, public authService: AuthService) { }

  ngOnInit(): void {
    this.equipment = {
      name: '',
      emailAddress: '',
      description: '',
      comments: [],
      postedBy: null,
    };
  }


  createEquipmentMethod(): void {
    console.log('create equipment called');

    this.equipment.postedBy = this.authService.currentUser$.getValue()
    console.log(this.equipment)

    console.log(this.authService.currentUser$.value.email)
    // this.equipment.emailAddress = this.authService.currentUser$.value.email;
    // this.equipment.postedBy = this.authService.currentUser$.value


    this.subscription = this.equipmentService.createEquipment(this.equipment)
    .subscribe(data => {
      console.log(data);
      this.location.back();
      alert(`Equipment ${this.equipment.name} was successfully created`)
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
