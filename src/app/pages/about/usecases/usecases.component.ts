import { Component, OnInit } from '@angular/core';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.css'],
})
export class UsecasesComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'User aanmaken',
      description: 'Hiermee kan een reguliere gebruiker een user aanmaken',
      scenario: ['Er komt een verzoek binnen om een user aan te maken.',
      'Het systeem valideert de inkomende gegevens.Alle vereiste gegevens dienen aanwezig te zijn.',
      'Het systeem valideert het token in de aanvraag',
      'Het systeem voegt de identificatie van de ingelogde gebruiker als administrator toe aan het nieuwe User '],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker heeft een valide token.',
      postcondition: 'De toegevoegde gebruiker is persistent opgeslagen.',
    },

    {
      id: 'UC-03',
      name: 'User wijzigen',
      description: 'Hiermee kan de user zijn gegevens wijzigen',
      scenario: ['Er komt een verzoek binnen om een user te wijzigen.',
       'Het systeem valideert het token in de aanvraag',
       'Het systeem valideert de inkomende gegevens.',
       'Het systeem zoekt het de te wijzigen user in het systeem.',
       'Het systeem valideert dat de juiste gebruiker het verzoek doet'],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker moet ingelogd zijn, De gebruiker heeft een valide token. De toe te voegen user dient te bestaan in het systeem.',
      postcondition: 'Het doel is bereikt.',
    },

    {
      id: 'UC-04',
      name: 'User deleten',
      description: 'Hiermee kan een administrator user verwijderen',
      scenario: ['Er komt een verzoek binnen om een user verwijderen.',
      'Het systeem valideert het token in de aanvraag', 'Het systeem zoekt het te verwijderen user in het systeem.',
      'Het systeem valideert dat de gebruiker die het verzoek doet ook de administrator van het user is',
      'Het systeem verwijdert het user.',
      'Het systeem stuurt een antwoordbericht terug, met daarin de gegevens van het verwijderde user.'],
      actor: this.PLAIN_USER,
      precondition: 'De gebruiker heeft een valide token. Het te verwijderen user bestaat in het systeem. De ingelogde gebruiker die het verzoek doet is de administrator van de user.',
      postcondition: 'De gegevens van de opgegeven user zijn verwijderd. De verwijdering is gepersisteerd in het systeem.',
    },


{
  id: 'UC-05',
  name: 'details-equipment',
  description: 'Hiermee kan een gebruiker de details van een equipement ophalen',
  scenario: ['Er komt een verzoek binnen om een de detail van een equipment op te halen.',
  'Het systeem zoekt de equipment op in het systeem.',
  'Het systeem stuurt een antwoordbericht terug, met daarin de gegevens van het equipment.'],
  actor: this.PLAIN_USER,
  precondition: 'De gebruiker moet ingelogd zijn',
  postcondition: 'equipment is opgehaald en word getoond in het systeem',
},

{
  id: 'UC-06',
  name: 'equipment delete',
  description: 'Hiermee kan een administrator equipment verwijderen',
  scenario: ['Er komt een verzoek binnen om een bepaalde equipment te verwijderen.',
  'Het systeem valideert het token in de aanvraag',
   'Het systeem zoekt het te verwijderen equipment in het systeem.',
   'Het systeem controleert of het de de administrator het equipment probeert te verwijderen',
  'Het systeem verwijdert het equipment',
  'Het systeem stuurt een antwoordbericht terug, met daarin de gegevens van het verwijderde equipment.'],
  actor: this.ADMIN_USER,
  precondition: 'De gebruiker heeft een valide token. Het te verwijderen user bestaat in het systeem. De ingelogde gebruiker die het verzoek doet is de administrator van de user.',
  postcondition: 'De gegevens van de opgegeven user zijn verwijderd. De verwijdering is gepersisteerd in het systeem.',
},

{
  id: 'UC-07',
  name: 'equipment update',
  description: 'Hiermee kan een administrator equipment updaten',
  scenario: ['Er komt een verzoek binnen om een bepaalde equipment te updaten.',
  'Het systeem valideert het token in de aanvraag',
   'Het systeem zoekt het te geupdate equipment in het systeem.',
   'Het systeem controleert of het de de administrator het equipment probeert te updaten',
  'Het systeem update de gegevens van het equipment',
  'Het systeem stuurt een antwoordbericht terug, met daarin de gegevens van het geupdate equipment.'],
  actor: this.ADMIN_USER,
  precondition: 'De gebruiker heeft een valide token. Het te geupdate equipment bestaat in het systeem. De ingelogde gebruiker die het verzoek doet is de administrator.',
  postcondition: 'De gegevens van de opgegeven user zijn geupdate. De update is gepersisteerd in het systeem.',
},

{
  id: 'UC-08',
  name: 'comment ophalen',
  description: 'Hiermee kan de administrator de comments ophalen',
  scenario: ['Er komt een verzoek binnen om een de detail van een comment op te halen.',
  'Het systeem zoekt de comment op in het systeem.',
  'Het systeem stuurt een antwoordbericht terug, met daarin de comment.'],
  actor: this.PLAIN_USER,
  precondition: 'De gebruiker moet ingelogd zijn',
  postcondition: 'comment is opgehaald en word getoond in het systeem',
},



{
  id: 'UC-09',
  name: 'lijst van comments',
  description: 'Hiermee kan de administrator de comments ophalen',
  scenario: ['Er komt een verzoek binnen om een de detail van een comment op te halen.',
  'Het systeem zoekt de comments op in het systeem.',
  'Het systeem stuurt een antwoordbericht terug, met daarin de comments.'],
  actor: this.PLAIN_USER,
  precondition: 'De gebruiker moet ingelogd zijn',
  postcondition: 'Het systeem heeft als antwoord een lijst van de gevraagde comments',
},


{
  id: 'UC-10',
  name: 'update van comment',
  description: 'Hiermee kan de gebruiker de comments wijzigen',
  scenario: ['Er komt een verzoek binnen om een de detail van een comment op te halen.',
  'Het systeem zoekt de comment op in het systeem.',
  'Het systeem stuurt een antwoordbericht terug, met daarin de comment.'],
  actor: this.PLAIN_USER,
  precondition: 'De gebruiker moet ingelogd zijn',
  postcondition: 'Het systeem heeft als antwoord de comment',
},


{
  id: 'UC-11',
  name: 'ophalen wishList',
  description: 'Hiermee kan de gebruiker de wishlist ophalen',
  scenario: ['Er komt een verzoek binnen om een de wishlist op te halen.',
  'Het systeem zoekt de wishList op in het systeem.',
  'Het systeem stuurt een antwoordbericht terug, met daarin de wishList'],
  actor: this.PLAIN_USER,
  precondition: 'De gebruiker moet ingelogd zijn',
  postcondition: 'Het systeem heeft als antwoord de wishList en toond het in het systeem',
},







  ];

  constructor() {}

  ngOnInit(): void {}
}
