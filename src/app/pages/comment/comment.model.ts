import { User } from "../user/user.model";

export class Comment {
  _id?: string;
  title: string;
  text: string;
  user: User;

}
