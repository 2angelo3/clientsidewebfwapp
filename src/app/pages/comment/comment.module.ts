import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentListComponent } from './comment-list/comment-list.component';
import { CommentDetailComponent } from './comment-detail/comment-detail.component';
import { CommentEditComponent } from './comment-edit/comment-edit.component';
import { RouterModule, Routes } from '@angular/router';
import { CommentAddComponent } from './comment-add/comment-add.component';
import { FormsModule } from '@angular/forms';
import { AuthGuard } from '../../auth/auth.guard';



const routes: Routes = [

  {
    path: ':equipid/editComment/:id', pathMatch: 'full', canActivate: [AuthGuard], component: CommentEditComponent
  },
  {
    path: ':equipid/add', pathMatch: 'full', canActivate: [AuthGuard], component: CommentAddComponent
  },
  {
    path: ':equipid', pathMatch: 'full', component: CommentListComponent
  },
  {
    path: ':equipid/:id', pathMatch: 'full', component: CommentDetailComponent
  }

];


@NgModule({
  declarations: [CommentListComponent, CommentDetailComponent, CommentEditComponent, CommentAddComponent],
  imports: [
    CommonModule, FormsModule, RouterModule.forChild(routes)
  ]
})
export class CommentModule { }
