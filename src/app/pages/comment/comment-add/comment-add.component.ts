import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CommentService } from '../comment.service';
import { Comment } from '../comment.model'
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { switchMap } from 'rxjs/operators';
import { Location } from '@angular/common';


@Component({
  selector: 'app-comment-add',
  templateUrl: './comment-add.component.html',
  styleUrls: ['./comment-add.component.css']
})
export class CommentAddComponent implements OnInit, OnDestroy {

  comment: Comment;

  subscription: Subscription;

  constructor(private commentService: CommentService, private location: Location, public authService: AuthService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.comment = {
      title: '',
      text: '',
      user: null
    };
  }


  createCommentMethod(): void {
    console.log('create comment called');
    this.comment.user = this.authService.currentUser$.getValue()

    console.log(this.authService.currentUser$.value._id)
    this.comment.user._id = this.authService.currentUser$.value._id;


    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        return this.commentService.createComment(params.get('equipid'), this.comment);
      })
    ).subscribe(comment => {
      console.log(comment);
      this.location.back();
      alert(`Comment ${this.comment.title} was successfully created`)
    })
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}



