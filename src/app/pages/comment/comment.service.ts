import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Comment } from 'src/app/pages/comment/comment.model';

const baseUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) { }

  getComments(equipid: string): Observable<Comment[]> {
    return this.http.get<Comment[]>(baseUrl + `equipment/${equipid}/comments`);
  }

  getCommentId(equipid: string, id: string): Observable<Comment> {
    return this.http.get<Comment>(baseUrl + `equipment/${equipid}/${id}`);
  }

  createComment(equipid: string, comment: Comment): Observable<Comment> {
    const headers = { 'content-type': 'application/json'};
    return this.http.post<Comment>(baseUrl + `equipment/${equipid}`, comment, {headers});
  }

  updateComment(equipid: string, comment: Comment): Observable<Comment> {
    const headers = { 'content-type': 'application/json'};
    console.log('id = ' + comment._id);
    return this.http.put<Comment>(baseUrl + `equipment/${equipid}/${comment._id}`, comment, {headers});
  }

  deleteComment(equipid: string, comment: Comment): Observable<Comment> {
    return this.http.delete<Comment>(baseUrl + `equipment/${equipid}/${comment._id}`);
  }

}
