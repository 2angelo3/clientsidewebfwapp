import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable, Subscription } from "rxjs";
import { CommentService } from "../comment.service";
import { Comment } from '../comment.model';
import { ActivatedRoute, ParamMap } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { textSpanIntersectsWithPosition } from "typescript";
import { AuthService } from "src/app/auth/auth.service";


@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit, OnDestroy {
  comments$: Observable<Comment[]>;
  subscription: Subscription;
  equipid: string;

  constructor(private commentService: CommentService, private route: ActivatedRoute, public authService: AuthService) { }
  ngOnDestroy(): void {
   if(this.subscription) this.subscription.unsubscribe()
  }

  ngOnInit(): void {
    console.log('userlist is loaded');
    //calls the method and show the users
   this.getComment();
  }


  getComment(): void {
    this.comments$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.equipid = params.get('equipid');
        return this.commentService.getComments(params.get('equipid'));
      })
    )
  }

  deleteComment(comment: Comment): void {
   this.subscription = this.commentService.deleteComment(this.equipid, comment).subscribe(comments => {
     console.log(comments);
      this.getComment();
    });
  }
}
