import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { from, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { CommentService } from '../comment.service';

import { Comment } from '../comment.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-comment-edit',
  templateUrl: './comment-edit.component.html',
  styleUrls: ['./comment-edit.component.css']
})
export class CommentEditComponent implements OnInit {

  comment: Comment;
  equipid: string;
  subscription: Subscription;



  constructor(private commentService: CommentService, private route: ActivatedRoute, private router: Router, public authService: AuthService) { }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  ngOnInit(): void {


    this.subscription = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => {
        this.equipid = params.get('equipid');
        return this.commentService.getCommentId(params.get('equipid'), params.get('id'));
      })
    ).subscribe(data => {
      this.comment = data;
    });


  }


  updateComment(): void {
    console.log(this.comment);

   this.subscription = this.commentService.updateComment(this.equipid, this.comment).subscribe(comments => {
    console.log(comments);
    this.router.navigate([`/comment/${this.equipid}`]);
  });
}
}
