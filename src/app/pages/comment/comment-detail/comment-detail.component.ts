import { Component, OnDestroy, OnInit } from "@angular/core";
import { Observable } from "rxjs/internal/Observable";
import { Subscription } from "rxjs/internal/Subscription";
import { CommentService } from "../comment.service";
import { Comment } from '../comment.model';
import { ActivatedRoute, ParamMap, Router } from "@angular/router";
import { switchMap } from "rxjs/operators";
import { AuthService } from "src/app/auth/auth.service";
import { Location } from "@angular/common";
import { AuthGuard } from "src/app/auth/auth.guard";


@Component({
  selector: 'app-comment-detail',
  templateUrl: './comment-detail.component.html',
  styleUrls: ['./comment-detail.component.css']
})
export class CommentDetailComponent implements OnInit, OnDestroy {

  comment: Comment;
  equipid: string;

  subscription: Subscription;

  constructor(private commentService: CommentService ,private router: Router, private route: ActivatedRoute, private location: Location, public authService: AuthService, public logged : AuthGuard) { }

  ngOnInit(): void {
    this.getComment();
  }

      getComment(): void {
        this.subscription = this.route.paramMap.pipe(
          switchMap((params: ParamMap) => {
            this.equipid = params.get('equipid');
            return this.commentService.getCommentId(params.get('equipid'), params.get('id'))
          })
        ).subscribe(comment => {
          this.comment = comment;
        })

      }



      deleteComment(comment: Comment): void {
        this.commentService.deleteComment(this.equipid, comment).subscribe(comments => {
          console.log(comments);
          this.getComment();
          this.location.back();
          // alert(`Comment ${this.comment.title} was successfully deleted`)
        });
      }

      ngOnDestroy(): void {
        if(this.subscription) this.subscription.unsubscribe();
      }


      tryToEdit(comment: Comment){
        console.log('currentUser', this.authService.currentUser$.value._id);
        console.log('commentUser', comment.user);
        if(this.logged.canActivate() === false){
          this.logged.canActivate();
        }else{
          if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(comment.user._id)){
            console.log(' allowed');
            this.router.navigateByUrl(`/comment/${this.equipid}/editComment/` + comment._id);
          }else{
            console.log('Not allowed');
          console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(comment._id)
            this.router.navigateByUrl('/login');
          }
        }
      }

      tryToDelete(comment: Comment){

        console.log(comment._id)
        console.log( JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(comment.user._id));
        if(this.logged.canActivate() === false){
          this.logged.canActivate();
        }else{
          if(JSON.stringify(this.authService.currentUser$.value._id) === JSON.stringify(comment.user._id)){
            console.log(' allowed');
            this.deleteComment(comment
              );
          }else{
            console.log('Not allowed');
            console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(JSON.stringify(this.authService.currentUser$.value._id));
            console.log(JSON.stringify(comment))
            this.router.navigateByUrl('/login');
          }
        }
      }




}
