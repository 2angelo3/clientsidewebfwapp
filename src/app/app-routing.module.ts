import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { LayoutComponent } from './layout/layout.component';
import { UsecasesComponent } from './pages/about/usecases/usecases.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'dashboard', pathMatch: 'full', component: DashboardComponent },
      { path: 'about', pathMatch: 'full', component: UsecasesComponent },
      {
        path: 'users',
        loadChildren: () =>
          import('./pages/user/user.module').then(m => m.UserModule)
      },
      {path: 'equipment',
      loadChildren: () =>
        import('./pages/equipment/equipment.module').then(e => e.EquipmentModule)
    },

    {path: 'wishlist',
    loadChildren: () =>
      import('./pages/wishlist/wishlist.module').then(w => w.WishlistModule)
  },

  {path: 'comment',
  loadChildren: () =>
    import('./pages/comment/comment.module').then(c => c.CommentModule)
},

    ],
  },

  { path: 'login', pathMatch: 'full', component: LoginComponent },
  { path: 'register', pathMatch: 'full', component: RegisterComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
